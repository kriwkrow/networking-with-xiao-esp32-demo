// RestApiCL.ino
// REST API with JSON Example (WiFi client version)
// Take a look here for access point info: https://wiki.seeedstudio.com/XIAO_ESP32C3_WiFi_Usage/#use-as-an-access-point
// Created 2024-04-24 by Krisjanis Rijnieks
//
// Learn more here:
// - https://restfulapi.net/
// - https://arduinojson.org/
// - https://github.com/lacamera/ESPAsyncWebServer
// 
// Note: ESPAsyncWebServer did not compile at first. You need to edit its source a little.
//       Uncomment or delete lines that cause issues as they are calls to nonexisting functions.

#ifdef ESP32
  #include <WiFi.h>
  #include <AsyncTCP.h>
#elif defined(ESP8266)
  #include <ESP8266WiFi.h>
  #include <ESPAsyncTCP.h>
#endif
 
#include <ESPAsyncWebServer.h>
#include "ArduinoJson.h"

#define MOT_PIN D2
#define LED_PIN D1

const char* ssid = "Fablab";
const char* password = "Fabricationlab1";
bool localState = false;

AsyncWebServer server(80);

void updateFromLocalState() {
  digitalWrite(MOT_PIN, localState);
  digitalWrite(LED_PIN, localState);
}

void setup() {
  Serial.begin(115200);

  pinMode(MOT_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi...");
  }
  
  Serial.println("Connected to WiFi");
  Serial.println();
  
  Serial.println("Access REST API: ");
  Serial.print("http://");
  Serial.println(WiFi.localIP());
  Serial.println();

  Serial.println("Toggle state ON/OFF");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/toggle");
  Serial.println();

  Serial.println("Switch ON");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/on");
  Serial.println();

  Serial.println("Switch OFF");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/off");
  Serial.println();

  Serial.println("Control with JSON");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/json?data={'state':'on'}");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.println("/json?data={'state':'off'}");
  Serial.println();

  // Hello world
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send(
      200, "text/plain", 
      "Welcome to ESP32 REST API! Available endpoints are /toggle /on /off /json?data={\"state\":\"on|off\"}"
    );
  });

  server.on("/toggle", HTTP_GET, [](AsyncWebServerRequest *request) {
    localState = !localState;
    updateFromLocalState();
    request->send(200, "application/json", "OK");
  });
  
  server.on("/on", HTTP_GET, [](AsyncWebServerRequest *request) {
    localState = true;
    updateFromLocalState();
    request->send(200, "application/json", "OK");
  });

  server.on("/off", HTTP_GET, [](AsyncWebServerRequest *request) {
    localState = false;
    updateFromLocalState();
    request->send(200, "application/json", "OK");
  });

  server.on("/json", HTTP_GET, [](AsyncWebServerRequest *request) 
  {
    if(request->hasParam("data")){
      String jsonInString = request->getParam("data")->value();
      Serial.print("Reveived data: ");
      Serial.println(jsonInString);

      char jsonInChar[jsonInString.length() + 1];
      jsonInString.toCharArray(jsonInChar, sizeof(jsonInChar));
      char* jsonInCharPtr = jsonInChar;
      
      JsonDocument jsonIn;
      deserializeJson(jsonIn, jsonInCharPtr);
      const char* state = jsonIn["state"];
      Serial.print("state: ");
      Serial.println(state);

      if (jsonIn["state"] == "on") {
        localState = true;
      } else if (jsonIn["state"] == "off") {
        localState = false;
      }

      updateFromLocalState();
    }

    JsonDocument jsonOut;
    jsonOut["status"] = "OK";
    
    String jsonOutString;
    serializeJson(jsonOut, jsonOutString);
 
    request->send(200, "application/json", jsonOutString);
  });

  // Start the server
  server.begin();
}

void loop() {
  // Nothing to do here, but probably reading and updating data
}

/*
  ESP-NOW Example
  Sends a on/off message to another XIAO ESP32 using ESP-NOW protocol  
  Created 2024-04-25 by Krisjanis Rijnieks
  Learn more at https://randomnerdtutorials.com/esp-now-esp32-arduino-ide/
*/

#include <esp_now.h>
#include "WiFi.h"

#define MOT_PIN D2
#define LED_PIN D1
#define BTN_PIN D0

bool localState = true;
bool btnReading = false;

// MAC #1: D4:F9:8D:04:21:BC
// MAC #2: D4:F9:8D:04:2B:14
//uint8_t broadcastAddress[] = {0xD4, 0xF9, 0x8D, 0x04, 0x2B, 0x14}; // sending to board #2
uint8_t broadcastAddress[] = {0xD4, 0xF9, 0x8D, 0x04, 0x21, 0xBC}; // sending to board #1

// set up your data structure here
typedef struct MyMessage {
  bool state;
} MyMessage;

MyMessage messageOut; // outgoing message
MyMessage messageIn;  // incomming message

esp_now_peer_info_t peerInfo;

// callback when data is sent
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
  Serial.print("\r\nLast Packet Send Status:\t");
  Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}

// callback when data is received
void OnDataRecv(const esp_now_recv_info_t *info, const uint8_t *incomingData, int len) {
  memcpy(&messageIn, incomingData, sizeof(messageIn));
  localState = messageIn.state;
  digitalWrite(MOT_PIN, messageIn.state);
  digitalWrite(LED_PIN, messageIn.state);
  
  Serial.print("Bytes received: ");
  Serial.println(len);
  Serial.print("State: ");
  Serial.println(messageIn.state);
  Serial.println();
}

void setup(){
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);

  if (esp_now_init() != ESP_OK) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  Serial.print("Device MAC address: ");
  Serial.println(WiFi.macAddress());

  // ESP-NOW is on. Register send and receive callbacks.
  esp_now_register_send_cb(OnDataSent);
  esp_now_register_recv_cb(OnDataRecv);
  
  // Register peer
  memcpy(peerInfo.peer_addr, broadcastAddress, 6);
  peerInfo.channel = 0;  
  peerInfo.encrypt = false;

  // Add peer
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }

  pinMode(MOT_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT);
  pinMode(BTN_PIN, INPUT_PULLUP);

  digitalWrite(MOT_PIN, localState);
  digitalWrite(LED_PIN, localState);
}
 
void loop()
{
  bool reading = digitalRead(BTN_PIN);
  
  if (!reading && reading != btnReading) 
  {  
    localState = !localState;

    digitalWrite(MOT_PIN, localState);
    digitalWrite(LED_PIN, localState);
    
    messageOut.state = localState;
    esp_err_t result = esp_now_send(broadcastAddress, (uint8_t *) &messageOut, sizeof(messageOut));

    if (result == ESP_OK) {
      Serial.println("Sent with success");
    } else {
      Serial.println("Error sending the data");
    }
  }
  
  btnReading = reading;
  
  delay(10);
}

# Wireless Networking with XIAO ESP32C3

This has been made for demoing purposes for the Fab Academy / Digital Fabrication courses.

![hero Shot](Images/Networking_XIAO_ESP32_Boards.jpg)

## Features

- KiCad board design (you will need two boards)
- Simple REST server with JSON capabilities
- ESP32-NOW example for "hard" connection between two XIAO ESP32 devices

## Demoing

The idea is to walk participants through the following.

- ESP32 chips in general
- Seeed XIAO ESP32C3 module
- KiCad design featuring a button, LED and vibration motor
- REST API example in client mode, showing where to find information about AP mode
- ESP-NOW example where boards form a connection between themselves

## Troubleshooting

When first installing the [ESPAsyncWebServer](https://github.com/lacamera/ESPAsyncWebServer/) library, you will not be able to compile due a few errors that need to be fixed in source code.

... TODO

## License

This code is public domain but you buy me a beer if you use this and we meet someday (Beerware license).

